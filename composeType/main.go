package main

import "fmt"

type hotdog int

var b hotdog = 10

func main() {
	fmt.Printf("%T", b)
	/* Convertendo */
	value := int(b)
	fmt.Printf("%v", value)
}
