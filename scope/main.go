package main

import "fmt"

/* Package scope - Can't use gopher*/
var name = "Leonardo"

func defineY() {
	/* Block scope - Should throw undefined*/
	fmt.Println(nameBlock)
}

func main() {

	/* Block scope - Can use gopher */

	nameBlock := "Bloise"

	fmt.Println(name)
	fmt.Println(nameBlock)

}
