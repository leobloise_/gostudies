package main

import "fmt"

type blusa int

var x blusa

func main() {
	fmt.Printf("%v %T\n", x, x)
	x = 42
	fmt.Printf("%v\n", x)
}
